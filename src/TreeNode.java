import java.util.*;

/*
 kasutatud allikad:
 http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
 http://pastebin.com/dfXRzaxD
 http://pastebin.com/yn25NWAc
 */


public class TreeNode {
	
   //================================================================================
   // Properties
   //================================================================================
	
   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   //================================================================================
   // Constructors
   //================================================================================
   
   TreeNode() {
   }
   
   TreeNode (String n, TreeNode d, TreeNode r) {
	   setName(n);
	   setFirstChild(d);
	   setNextSibling(r);
   }
   
   //================================================================================
   // Getters & Setters
   //================================================================================
   
   public void setName (String n){
	   name = n;
   }
   
   public void setFirstChild (TreeNode d){
	   firstChild = d;
   }
   
   public void setNextSibling (TreeNode r){
	   nextSibling = r;
   }
   
   public String getName(){
	   return name;
   }
   
   public TreeNode getFirstChild(){
	   return firstChild;
   }
   
   public TreeNode getNextSibling(){
	   return nextSibling;
   }
   
   //================================================================================
   // Tests
   //================================================================================
   
   public static void testInput (String s) {
	 //testEmptyString
	   if (s.isEmpty()) { 
		   throw new RuntimeException("viga: tühi sisend");
	   } 
	 //testTab
	   if (s.contains("\t")) { 
		   throw new RuntimeException("viga: sisendis ei tohi kasutada tab'i");
	   }
	 //testEmptySubtree
	   if (s.contains("()")){ 
		   throw new RuntimeException("viga: sisendis ei tohi esineda tühjad sulud, sisend: " + s);
	   } 
	 //testTwoCommas
	   if (s.contains(",,")) { 
		   throw new RuntimeException("viga: sisendis ei tohi olla kaks koma järjestiku, sisend: " + s);
	   }
	 //testSpaceInTreeNodeName
	   if (s.contains(" ")) { 
		   throw new RuntimeException("viga: sisendis ei tohi esineda tühikuid, sisend: " + s);
	   } 
	 //testInputWithoutBrackets
	   if (!s.contains("(") && !s.contains(")") && s.contains(",")) { 
		   throw new RuntimeException("viga: sisendil puudub tipp, sisend: " + s);
	   } 
	 //testInputWithDoubleBrackets 
	   if (s.contains("((") && s.contains("))")) { 
		   throw new RuntimeException("viga: sisendis on topeltsulud, sisend: " + s);
	   } 
	 //testUnbalanced
	   testIfBalanced(s); 
	 //testCommas 1+2+3
	   testCommas(s); 
   }
   
   //abimeetod komade testimiseks.
   public static void testCommas (String s) {
	   	   
	   for (int i = 0; i < s.length(); i++) {
		   if (s.charAt(i) == ',') {
			   if (s.charAt(i-1) == '('){
				   throw new RuntimeException("viga: algava sulu järel ei saa olla koma, sisend: " + s);
			   } else if (s.charAt(i+1) == ')'){
				   throw new RuntimeException("viga: lõppeva sulu ees ei saa olla koma, sisend: " + s);
			   }
		   }
	   }
	   
	   int avatudSulg = 0;
	   for (int i = 0; i < s.length(); i++) {
		   if (s.charAt(i) == '('){
			   avatudSulg++;
		   } else if (s.charAt(i) == ')') {
			   avatudSulg--;
		   } else if (s.charAt(i) == ',' && avatudSulg == 0){
			   throw new RuntimeException("viga: lehel '" + s.charAt(i+1) + "' puudub tipp " + ", sisend: " + s);
		   }
	   }
   }
   
   //abimeetod avaldise sulgude tasakaalu testimiseks.
   public static void testIfBalanced (String s){
	   int countRight = 0;
	   int countLeft = 0;

	   for (int i = 0; i < s.length(); i++) {
		   if (s.charAt(i) == ')'){
			   countRight++;
		   } else if (s.charAt(i) == '('){
			   countLeft++;
		   }
	   }
	   if (countRight != countLeft){
		   throw new RuntimeException("viga: sisendi sulud ei ole tasakaalus, sisend: " + s);
	   }
   }
   
   //================================================================================
   // Tree Generator
   //================================================================================
   
   public static TreeNode parsePrefix (String s) {
	   
	   testInput(s); //kontrollime kas sisend on korrektne
	   return generateTree(s);
   }
   
   private static TreeNode generateTree(String s){
	   
	   if (!s.contains("(") && !s.contains("(") && !s.contains("(")){ //kontrollime kas on üksik node
		   return new TreeNode(s, null, null);
	   } else {
		   
	        TreeNode output = new TreeNode();

		    String firstChild = ""; 
	        String nextSibling = "";
	        String delim = "";
	        StringTokenizer st = new StringTokenizer(s,"(),",true);
	  /* StringTokenizer(String str, String delim, boolean returnDelims)
	  
		Constructs a string tokenizer for the specified string. 
		All characters in the delim argument are the delimiters for separating tokens.
		
		- If the returnDelims flag is TRUE, then the delimiter characters are also returned as tokens. 
		Each delimiter is returned as a string of length one. 
		- If the flag is FALSE, the delimiter characters are skipped and only serve as separators between tokens.
	  */
	        output.setName(st.nextToken()); //esimene token on alati root
	        int numOfOpenBrackets = 0; 

	        while(st.hasMoreTokens()) {	 //while loop käimaks läbi kõik tokenid
	        	delim = st.nextToken(); 
	            if (delim.equals("(")) { 
	                if (numOfOpenBrackets==0) { 
	                	firstChild=st.nextToken();
	                } else { 
	                	firstChild+=delim+st.nextToken();
	                }
	                numOfOpenBrackets++;
	            } else if (delim.equals(",")) {
	                if (numOfOpenBrackets!=0) { 
	                	firstChild+=delim+st.nextToken();
	                } else while (st.hasMoreTokens()) {
	                	nextSibling+=st.nextToken();
	                }
	            } else if (delim.equals(")")) { 
	            	numOfOpenBrackets--; 
	                if (numOfOpenBrackets>0) {
	                	firstChild+=delim;
	                }
	            }
	        }
	        if(firstChild!=""){ //peab kontrollima, tühi stringi kaasamine tooks kaasa anomaaliaid hiljem
		        output.setFirstChild(generateTree(firstChild));
	        }
	        if(nextSibling!=""){ //peab kontrollima, tühi string tekitab anomaaliaid
	        	output.setNextSibling(generateTree(nextSibling));
	        }
	        return output;
	   }
	   	   
   }

   //================================================================================
   // toStrings
   //================================================================================
   
   public String rightParentheticRepresentation() {
	   StringBuffer b = new StringBuffer();
	     
	      TreeNode node = this.getFirstChild();
	     
	      if (node != null) {
	          b.append("("); //algab alati suluga.
	      }
	     
	      while (node != null) { 
	          b.append(node.rightParentheticRepresentation());
	         
	          if(node.nextSibling!=null) {
	                  b.append(","); //kui on veel naabreid, siis lisame koma.
	          } else {
	                  b.append(")"); //kui naabreid ei leidu, sulgeme sulu
	          }
	         
	          node = node.getNextSibling();
	      }
	         
	      b.append(this.name); //root on alati viimane
	     
	      return b.toString();
   }
   
   //lisameetod testimiseks kas eelnevalt genereeritud TreeNode on korrektne. 
   //kui see meetod edukalt funktsioneerib, siis on teada, et puu on õigesti genereeritud ja
   //võib alustada rightParentheticRepresentation() meetodiga
   public String leftParenthicRepresentation() {
	   String result = this.name;
	   
	   if (firstChild != null) {
		   result += "(" + firstChild.leftParenthicRepresentation() + ")";
	   }
	   if (nextSibling != null) {
		   result += "," + nextSibling.leftParenthicRepresentation();
	   }
	   return result;
   }

   //================================================================================
   // Main
   //================================================================================
   
   public static void main (String[] param) {
      
	  //String s = "+(*(-(512,1),4),/(-6,3))";
	  //String s = "A(B(C(D(E))))";
	  //String s = "6(5(1,3(2),4))";
	  String s = "A(B1,C,D)";
      TreeNode t = TreeNode.parsePrefix (s);
      //String x = t.leftParenthicRepresentation();
      //System.out.println (s + " ==> " + x); // A(B1,C,D) ==> A(B1,C,D)
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }
}
